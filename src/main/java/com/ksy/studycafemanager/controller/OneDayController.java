package com.ksy.studycafemanager.controller;

import com.ksy.studycafemanager.model.OneDayItem;
import com.ksy.studycafemanager.model.OneDayRequest;
import com.ksy.studycafemanager.model.OneDaySeatUpdateRequest;
import com.ksy.studycafemanager.model.OneDayTimeUpdateRequest;
import com.ksy.studycafemanager.service.OneDayService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/one-day")
public class OneDayController {
    private final OneDayService service;

    @PostMapping("/data")
    public String SetData(@RequestBody @Valid OneDayRequest request) {
        service.setData(request);

        return "등록되었습니다.";
    }

    @GetMapping("/all")
    public List<OneDayItem> getGuests() {
        return service.getGuests();
    }

    @PutMapping("/seat/id/{id}")
    public String putSeat(@PathVariable long id, @RequestBody @Valid OneDaySeatUpdateRequest request) {
        service.putSeat(id, request);

        return "좌석 수정되었습니다.";
    }

    @PutMapping("/time/id/{id}")
    public String putTime(@PathVariable long id, @RequestBody @Valid OneDayTimeUpdateRequest request) {
        service.putTime(id, request);

        return "시간 수정되었습니다.";
    }

    @PutMapping("/out/id/{id}")
    public String putOut(@PathVariable long id) {
        service.putOut(id);

        return "퇴실하셨습니다.";
    }

    @DeleteMapping("/sign-out/id/{id}")
    public String delData(@PathVariable long id) {
        service.delData(id);

        return "삭제되었습니다.";
    }
}
