package com.ksy.studycafemanager.controller;

import com.ksy.studycafemanager.model.*;
import com.ksy.studycafemanager.service.RegularService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v2/regular")
public class RegularController {
    private final RegularService service;

    @PostMapping("/data")
    public String setData(@RequestBody @Valid RegularRequest request) {
        service.setData(request);

        return "등록 완료";
    }

    @GetMapping("/all")
    public List<RegularItem> getRegulars() {
        return service.getRegulars();
    }

    @GetMapping("/guest/id/{id}")
    public RegularResponse getRegular(@PathVariable long id) {
        return service.getGuest(id);
    }

    @PutMapping("/test/id/{id}")
    public String putTestGet(@PathVariable long id, @RequestBody @Valid RegularTestUpdateRequest request) {
        service.putTestGet(id, request);

        return "갱신 알림 테스트를 위해 시작 날짜를 바꿔봤습니다.";
    }

    @PutMapping("/info/id/{id}")
    public String putInfo(@PathVariable long id, @RequestBody @Valid RegularInfoUpdateRequest request) {
        service.putInfo(id, request);

        return "개인정보가 수정되었습니다.";
    }

    @PutMapping("/regular/id/{id}")
    public String putWeek(@PathVariable long id, @RequestBody @Valid WeekUpdateRequest request) {
        service.putWeek(id, request);

        return "이용권이 수정되었습니다.";
    }

    @DeleteMapping("/sign-out/id/{id}")
    public String delData(@PathVariable long id) {
        service.delData(id);

        return "삭제되었습니다.";
    }
}
