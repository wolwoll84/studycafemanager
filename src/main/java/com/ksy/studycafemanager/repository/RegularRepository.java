package com.ksy.studycafemanager.repository;

import com.ksy.studycafemanager.entity.Regular;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegularRepository extends JpaRepository<Regular, Long> {
}
