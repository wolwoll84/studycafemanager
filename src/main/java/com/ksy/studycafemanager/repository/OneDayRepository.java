package com.ksy.studycafemanager.repository;

import com.ksy.studycafemanager.entity.OneDay;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OneDayRepository extends JpaRepository<OneDay, Long> {
}
