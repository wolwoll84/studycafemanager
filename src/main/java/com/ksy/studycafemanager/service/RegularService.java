package com.ksy.studycafemanager.service;

import com.ksy.studycafemanager.entity.Regular;
import com.ksy.studycafemanager.model.*;
import com.ksy.studycafemanager.repository.RegularRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RegularService {
    private final RegularRepository repository;

    public void setData(RegularRequest request) {
        Regular addData = new Regular();
        addData.setGuestName(request.getGuestName());
        addData.setGuestPhone(request.getGuestPhone());
        addData.setSeat(request.getSeat());
        addData.setUseWeek(request.getUseWeek());
        addData.setRegularStart(LocalDateTime.now());
        addData.setRegularEnd(LocalDateTime.now().plusWeeks(request.getUseWeek()));
        addData.setHaveLocker(request.getHaveLocker());

        repository.save(addData);
    }

    public List<RegularItem> getRegulars() {
        List<RegularItem> result = new LinkedList<>();

        List<Regular> originData = repository.findAll();

        for (Regular item : originData) {
            RegularItem addItem = new RegularItem();
            addItem.setId(item.getId());
            addItem.setGuestName(item.getGuestName());
            addItem.setSeat(item.getSeat());
            addItem.setUseWeek(item.getUseWeek());
            addItem.setRegularSeason(item.getRegularStart() + " ~ " + item.getRegularEnd());
            addItem.setHaveLocker(item.getHaveLocker());

            result.add(addItem);
        }

        return result;
    }

    public RegularResponse getGuest(long id) {
        Regular originData = repository.findById(id).orElseThrow();

        RegularResponse result = new RegularResponse();
        result.setId(originData.getId());
        result.setGuestInfo(originData.getGuestName() + "님 / " + originData.getGuestPhone() + " / " + originData.getSeat() + "번 좌석");
        result.setPeriodUse(originData.getUseWeek() + "주 이용권 / " + originData.getRegularStart() + "부터 " + originData.getRegularEnd() + "까지");
        result.setRenewalRequired(LocalDateTime.now().isAfter(originData.getRegularEnd().minusDays(3)) ? "갱신 필요" : null); // 정기권은 LocalDate 를 쓰는 게 깔끔하겠다
        result.setRenewalRequired2(originData.getRegularEnd().minusDays(3).isBefore(LocalDateTime.now()) ? "갱신 필요" : null);
        result.setHaveLocker(originData.getHaveLocker()); // 종료일 3일 전부터 쭈우우욱 갱신하시오로 뜨게? 3일 전 이후야? 라고 물어보자!! 현재 날짜가? 필요할까? 성공!!!

        return result;
    }

    public void putTestGet(long id, RegularTestUpdateRequest request) {
        Regular originData = repository.findById(id).orElseThrow();
        originData.setRegularStart(request.getRegularStart());
        originData.setRegularEnd(request.getRegularStart().plusWeeks(originData.getUseWeek()));

        repository.save(originData);
    }

    public void putInfo(long id, RegularInfoUpdateRequest request) {
        Regular originData = repository.findById(id).orElseThrow();
        originData.setGuestName(request.getGuestName());
        originData.setGuestPhone(request.getGuestPhone());

        repository.save(originData);
    }
    
    public void putWeek(long id, WeekUpdateRequest request) {
        Regular originData = repository.findById(id).orElseThrow();
        originData.setUseWeek(request.getUseWeek());
        originData.setRegularEnd(originData.getRegularStart().plusWeeks(request.getUseWeek()));
        repository.save(originData);
    }

    public void delData(long id) {
        repository.deleteById(id);
    }
}
