package com.ksy.studycafemanager.service;

import com.ksy.studycafemanager.entity.OneDay;
import com.ksy.studycafemanager.model.OneDayItem;
import com.ksy.studycafemanager.model.OneDayRequest;
import com.ksy.studycafemanager.model.OneDaySeatUpdateRequest;
import com.ksy.studycafemanager.model.OneDayTimeUpdateRequest;
import com.ksy.studycafemanager.repository.OneDayRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OneDayService {
    private final OneDayRepository repository;

    public void setData(OneDayRequest request) {
        OneDay addData = new OneDay();
        addData.setGuestName(request.getGuestName());
        addData.setGuestPhone(request.getGuestPhone());
        addData.setSeat(request.getSeat());
        addData.setUseTime(request.getUseTime());
        addData.setTimeStart(LocalDateTime.now());
        addData.setTimeEnd(LocalDateTime.now().plusHours(request.getUseTime()));

        repository.save(addData);
    }

    public List<OneDayItem> getGuests() {
        List<OneDay> originData = repository.findAll();

        List<OneDayItem> result = new LinkedList<>();

        for (OneDay item : originData) {
            OneDayItem addData = new OneDayItem();
            addData.setId(item.getId());
            addData.setGuestName(item.getGuestName());
            addData.setSeat(item.getSeat());
            addData.setUseTime(item.getUseTime());
            addData.setTimeTotal(item.getTimeStart() + " ~ " + item.getTimeEnd());
            addData.setIsOut(item.getTimeOut() == null ? false : true);

            result.add(addData);
        }

        return result;
    }

    public void putSeat(long id, OneDaySeatUpdateRequest request) {
        OneDay originData = repository.findById(id).orElseThrow();
        originData.setSeat(request.getSeat());

        repository.save(originData);
    }

    public void putTime(long id, OneDayTimeUpdateRequest request) {
        OneDay originData = repository.findById(id).orElseThrow();
        originData.setUseTime(request.getUseTime());
        originData.setTimeEnd(originData.getTimeStart().plusHours(request.getUseTime()));

        repository.save(originData);
    }

    public void putOut(long id) {
        OneDay originData = repository.findById(id).orElseThrow();
        originData.setTimeOut(LocalDateTime.now());

        repository.save(originData);
    }

    public void delData(long id) {
        repository.deleteById(id);
    }
}
