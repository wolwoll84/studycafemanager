package com.ksy.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class OneDaySeatUpdateRequest {
    @NotNull
    private Integer seat;
}
