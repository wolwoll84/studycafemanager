package com.ksy.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegularResponse {
    private Long id;
    private String guestInfo;
    private String periodUse;
    private String renewalRequired;
    private String renewalRequired2;
    private Boolean haveLocker;
}
