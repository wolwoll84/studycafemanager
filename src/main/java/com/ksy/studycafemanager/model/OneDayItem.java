package com.ksy.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class OneDayItem {
    private Long id;
    private String guestName;
    private Integer seat;
    private Integer useTime;
    private String timeTotal;
    private Boolean isOut;
}
