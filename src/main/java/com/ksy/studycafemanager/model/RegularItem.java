package com.ksy.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class RegularItem {
    private Long id;
    private String guestName;
    private Integer seat;
    private Integer useWeek;
    private String regularSeason;
    private Boolean haveLocker;
}
