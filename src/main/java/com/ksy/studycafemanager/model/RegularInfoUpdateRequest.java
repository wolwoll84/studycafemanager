package com.ksy.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class RegularInfoUpdateRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String guestName;

    @NotNull
    @Length(min = 7, max = 20)
    private String guestPhone;
}
