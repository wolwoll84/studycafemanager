package com.ksy.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class RegularRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String guestName;

    @NotNull
    @Length(min = 7, max = 20)
    private String guestPhone;

    @NotNull
    private Integer seat;

    @NotNull
    @Min(value = 2)
    @Max(value = 60)
    private Integer useWeek;

    @NotNull
    private Boolean haveLocker;
}
