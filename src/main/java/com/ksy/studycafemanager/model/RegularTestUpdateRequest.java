package com.ksy.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class RegularTestUpdateRequest {
    @NotNull
    private LocalDateTime regularStart;
}
