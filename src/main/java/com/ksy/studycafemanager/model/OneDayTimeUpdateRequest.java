package com.ksy.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class OneDayTimeUpdateRequest {
    @NotNull
    @Min(value = 1)
    @Max(value = 12)
    private Integer useTime;
}
