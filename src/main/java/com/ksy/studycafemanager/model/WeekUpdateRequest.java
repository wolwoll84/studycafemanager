package com.ksy.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class WeekUpdateRequest { // 이름 잘못 지었어 앞에 Regular 붙일걸
    @NotNull
    @Min(value = 2)
    @Max(value = 60)
    private Integer useWeek;
}
