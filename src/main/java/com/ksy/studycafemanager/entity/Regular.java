package com.ksy.studycafemanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Regular {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String guestName;

    @Column(nullable = false, length = 20)
    private String guestPhone;

    @Column(nullable = false)
    private Integer seat;

    @Column(nullable = false)
    private Integer useWeek;

    @Column(nullable = false)
    private LocalDateTime regularStart;

    @Column(nullable = false)
    private LocalDateTime regularEnd;

    @Column(nullable = false)
    private Boolean haveLocker;
}
